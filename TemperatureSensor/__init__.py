import re

class TempSensor():
    _DRIVER = "/sys/bus/w1/devices/28-%s/w1_slave"
    _TEMP_PATTERN = re.compile("t=(\d+)")
    _ERROR_TEMP = -999.99

    def __init__(self, sensor_id):
        self._id = sensor_id

    def _read_data(self):
        data_file = open(self._DRIVER % (self._id, ), "r")
        try:
            return data_file.read()
        finally:
            data_file.close()

    def get_temperature(self):
        data = self._read_data()
        m = self._TEMP_PATTERN.search(data)
        if not m:
            return _ERROR_TEMP
        return str(int(round(float(m.group(1)) / 1000)))
