#!/usr/bin/python
# -*- coding: utf-8 -*-

import ConfigParser
import rrdtool
import datetime
import RPi.GPIO as GPIO
from lcdscreen import LCDScreen

def day_name(key):
	# Format the date to be comparible & get today and tomorrow's date
	date_check = key.strftime('%y-%m-%d')
	today = datetime.date.today()
	tomorrow = today + datetime.timedelta(days=1)
	# If the date is the same as either today and tomorrow, display those words
	# Otherwise, just show the day name
	if  date_check == today.strftime('%y-%m-%d') :
		day_format = 'Today'
	elif date_check == tomorrow.strftime('%y-%m-%d'):
		day_format = 'Tomorrow'
	else:
		day_format = key.strftime('%A')

	return day_format

def nice_temp(temp):
	temp = str(int(round(float(temp))))
	return '%s%sc' % (temp, unichr(223))

# Get config
config = ConfigParser.RawConfigParser()
config.read('/home/pi/WeatherStation/_config.cfg')

# Button Set Up
button = 14
GPIO.setmode(GPIO.BCM)
GPIO.setup(button, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# Intialise the screen
lcd = LCDScreen({
	'delay': 5
})

# Start this baby up! Continuously loop and listen
while True:
	input_state = GPIO.input(button)

	# Is the button pressed?
	if input_state == False:

		# Get latest forecast data
		forecast = ConfigParser.RawConfigParser()
		forecast.read(config.get('files', 'forecast'))

		# Get latest temperature data
		temperature = rrdtool.info(config.get('files', 'temperature'))
		outdoor = temperature['ds[outdoor].last_ds']
		indoor = temperature['ds[indoor].last_ds']

		# Turn on the screen
		lcd.backlight('on')

		# Get current weather
		for (key, value) in forecast.items('currently'):
			currently = value.split('|')
			current_weather = currently[0]

		# Display Temp and Current Weather
		lcd.message(lcd.spaced('Inside:', nice_temp(temperature['ds[indoor].last_ds'])))
		lcd.message(lcd.spaced('Outside:', nice_temp(temperature['ds[outdoor].last_ds'])))
		lcd.message('Currently:')
		lcd.message(current_weather)
		lcd.delay_clear()

		# Get the dily forecasts for the next 3 days
		forecast_daily = forecast.items('daily')
		forecast_daily = forecast_daily[:3]

		for (key, value) in forecast_daily:
			# Gey the date time and split the value into a list
			key = datetime.datetime.fromtimestamp(int(key))
			details = value.split('|')

			# Output it a day at a time
			lcd.message(day_name(key), 'center', '-')
			lcd.message(details[0])
			lcd.message('High: %s at %s' % (nice_temp(details[3]), datetime.datetime.fromtimestamp(int(details[4])).strftime('%l%P')))
			lcd.message('Low:  %s at %s' % (nice_temp(details[1]), datetime.datetime.fromtimestamp(int(details[2])).strftime('%l%P')))
			lcd.delay()

		# Get the hourly forecasts for the next 3 days
		lcd.clear()
		forecast_hourly = forecast.items('hourly')
		weather_day = 0
		new_weather = ' '

		for (key, value) in forecast_hourly:

			# Gey the date time and split the value into a list
			key = datetime.datetime.fromtimestamp(int(key))
			details = value.split('|')

			# Push up the latest forecast
			lcd.push_up(new_weather)
			lcd.delay(1)

			new_weather_day = key.strftime('%j')

			if new_weather_day != weather_day :
				lcd.push_up(day_name(key), 'center', '-')

				weather_day = new_weather_day
				lcd.delay_microseconds(500)

			new_weather = lcd.spaced(lcd.truncate('%s %s' % (key.strftime('%l%P'), details[0]), 15), nice_temp(details[1]))
			lcd.delay_microseconds(500)

		lcd.delay_clear()
		lcd.backlight('off')
