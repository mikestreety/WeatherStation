#!/usr/bin/python

# Import modules
import forecastio
import ConfigParser
import os

# Load config file
config = ConfigParser.RawConfigParser()
config.read('/home/pi/WeatherStation/_config.cfg')

# Open the forecast file
forecast = ConfigParser.RawConfigParser()
forecast.read(config.get('files', 'forecast'))

# Initialise the Forecastion API
forecast_api = forecastio.load_forecast(
	config.get('forecastio', 'api_key'),
	config.get('forecastio', 'location_lat'),
	config.get('forecastio', 'location_lng')
)

'''
How this module works:

Each different forecast type (currently, daily, hourly) has a section
in the forecast.cfg file.

This file should be run on a cron job - every 10 minutes or so (but no
more than 40 times an hour)

At the beginning of each type, the previous data is deleted if it exits
(in case the file gets cleared). There is then the appropriate data added
for each type in a comma seperated list. The list becomes the value and
the key is the unix timestamp. Different types have different data
availible - hence it has to be specified each time.

Its all added to the forecast file opbject, then written to at the end.
'''

# Current data
if forecast.has_section('currently'):
	forecast.remove_section('currently')

forecast.add_section('currently')

c = forecast_api.currently()
forecast.set('currently', c.time.strftime('%s'), '%s|%s' % (
	c.summary,
	c.apparentTemperature
))

# Daily Forecasts
if forecast.has_section('daily'):
	forecast.remove_section('daily')

forecast.add_section('daily')

daily = forecast_api.daily()
for d in daily.data:
	forecast.set('daily', d.time.strftime('%s'), '%s|%s|%s|%s|%s' % (
		d.summary,
		d.apparentTemperatureMin,
		d.apparentTemperatureMinTime,
		d.apparentTemperatureMax,
		d.apparentTemperatureMaxTime
	))

# Hourly Forecasts
if forecast.has_section('hourly'):
	forecast.remove_section('hourly')

forecast.add_section('hourly')

hourly = forecast_api.hourly()
for h in hourly.data:
	forecast.set('hourly', h.time.strftime('%s'), '%s|%s' % (h.summary, h.apparentTemperature))

# Write the data
with open(config.get('files', 'forecast'), 'w') as f:
    forecast.write(f)
