# Weather Station Raspberry Pi

This is the code powering my Weather Station composed of a Raspberry Pi and hardware.

**This repo assumes you have downloaded the code to `/home/pi/WeatherStation`**

## Installation

Download the repo to the home directory and install the following python modules:

From apt-get:

```
sudo apt-get update
sudo apt-get install rrdtool
sudo apt-get install python-dev
sudo apt-get install python-rpi.gpio
sudo apt-get install python-rrd
sudo apt-get install python-pip
```

Or as a one line:

```
sudo apt-get install rrdtool python-dev python-rpi.gpio python-rrd python-pip
```

Install from pip:

```
sudo pip install configparser
sudo pip install python-forecastio
sudo pip install lcdscreen
```

Or as a one line:

```
sudo pip install configparser python-forecastio lcdscreen
```


## Setup

There are three files that need creating, the Round Robin Database, the forecast file and the config file.

### Forecast File

In the `data` folder, create an empty `forecast.cfg` file.

### Config File

In the root of the project, create a file called `_config.cfg`. It needs to have the following sections and keys

```
[sensors]
sensor_name = sensor_id
second_sensor = sensor_id

[files]
temperature = /path/to/the/database.rrd
forecast = /path/to/forecast/file.cfg

[forecastio]
api_key = apikey
location_lat = 1.123
location_lng = 1.123
```

Obtain your API key from [forecastio](https://developer.forecast.io/)

### Round Robin Database (RRD)

The RRD tool is just made through the command line:

```
rrdtool create data/temperature.rrd \
    —start now —step 300 \
    DS:indoor:GAUGE:120:-50:50 \
    DS:outdoor:GAUGE:120:-50:50 \
    RRA:AVERAGE:0.5:1:12 \
    RRA:AVERAGE:0.5:1:288 \
    RRA:AVERAGE:0.5:12:168 \
    RRA:AVERAGE:0.5:12:720 \
    RRA:AVERAGE:0.5:288:365
```

(Note the `DS` rows which are the actual columns. There is a blog post coming!)

### Set up the Daemon

The daemon file is included in the repo, but needs to be set up correctly:

```
sudo cp /home/pi/WeatherStation/Daemon.sh /etc/init.d/weatherstation.sh
sudo ln -s /home/pi/WeatherStation/WeatherStation.py /usr/local/bin/WeatherStation.py
```

### Set up the Cron

Add the temperature and forecast Get files to the crontab. Edit the crontab file:

```
sudo crontab -e
```

And add the following lines:

```
*/10 * * * * sudo /usr/bin/python /home/pi/WeatherStation/GetForecast.py
* * * * * sudo /usr/bin/python /home/pi/WeatherStation/GetTemperature.py
```

## Get Started

Run the `GetTemperature.py` and `GetForecast.py` first, to make sure there is some data.

```
sudo python /home/pi/WeatherStation/GetForecast.py
sudo python /home/pi/WeatherStation/GetTemperature.py
```

After that, start the WeatherStation, run the following code:

```
sudo /etc/init.d/weatherstation.sh start
```

If you want to see if the process is running, you can use:

```
sudo /etc/init.d/weatherstation.sh status
```

And if you want to stop the process, you can run

```
sudo /etc/init.d/weatherstation.sh stop
```

## Run on boot

Lastly, if you want the service to run on boot (for example in a power cut), you can add the service to the boot script:

```
sudo update-rc.d weatherstation.sh defaults
```