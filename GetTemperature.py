#!/usr/bin/python

# Import tools
import rrdtool
import os
import ConfigParser
from TemperatureSensor import TempSensor

# Load config file
config = ConfigParser.RawConfigParser()
config.read('/home/pi/WeatherStation/_config.cfg')

# Initiate 1 Wire for them readings
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

head = ''
data = 'N:'

for (key, value) in config.items('sensors'):
	head += '%s:' % (key)
	data += '%s:' % (TempSensor(value).get_temperature())

head = head[:-1]
data = data[:-1]

# Add the data to the Round Robin Database
rrdtool.update(config.get('files', 'temperature'), '--template', head, data)
